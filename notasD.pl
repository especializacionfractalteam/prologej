
estudiante('Diego').
estudiante('Maria').
estudiante('Juan').
estudiante('Diana').

parcial1('Diego',4.4).
parcial1('Maria',5.0).
parcial1('Juan',3.3).
parcial1('Diana',2.4).

parcial2('Diego',3.9).
parcial2('Maria',3.0).
parcial2('Juan',2.3).
parcial2('Diana',1.4).

parcial3('Diego',2.9).
parcial3('Maria',2.7).
parcial3('Juan',3.4).
parcial3('Diana',2.7).

porcentaje_parcial1(0.25).
porcentaje_parcial2(0.25).
porcentaje_parcial3(0.5).
 

porpar1(I,Y):-
        parcial1(I,X),
        Y is (X*25)/5.

porpar2(I,Y):-
        parcial2(I,X),
        Y is (X*25)/5.

porpar3(I,Y):-
        parcial3(I,X),
        Y is (X*50)/5.

definitiva(W,X,Y,Z):-
        Z is  ((W + X + Y)*5)/100.

notafinal(X,Y):-
	X < 3 -> Y = reprobo; 
	true -> Y = aprobo.

reprobado(I):-  porpar1(I,X), 
                porpar2(I,Y), 
                porpar3(I,Z),
                definitiva(X,Y,Z,W),
                notafinal(W,R),
                R=reprobo.
aprobado(X):-   parcial1(X,Z),
                notafinal(Z,W),
                W=aprobo.    % teniendo calculonota aca se cambiaria parcial1 por calculanota 

