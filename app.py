from pyswip import Prolog

prolog = Prolog()
prolog.consult("notas.pl")

for result in prolog.query("estudiante(X)"):
	print  "Estudiantes: "+result['X']

print

print "---------Notas Parcial 1---------"
for result in prolog.query("porcentaje_parcial1(X)"):
	print "Porcentaje del parcial "+str(result["X"])

print 

for result in prolog.query("parcial1(X,Y)"):
	print "Estudiante "+result["X"]+" Nota: "+ str(result["Y"])

print

print "Aprobaron el primer parcial:"
for result in prolog.query("aprobado1(X)"):
	print result["X"] 

print

print "---------Notas Parcial 2---------"
for result in prolog.query("porcentaje_parcial2(X)"):
	print "Porcentaje del parcial "+str(result["X"])

print

for result in prolog.query("parcial2(X,Y)"):
	print "Estudiante "+result["X"]+" Nota: "+ str(result["Y"])


print

print "Aprobaron el segundo parcial:"
for result in prolog.query("aprobado2(X)"):
	print result["X"] 

print

print "---------Notas Parcial 3---------"
for result in prolog.query("porcentaje_parcial3(X)"):
	print "Porcentaje del parcial "+str(result["X"])

print

for result in prolog.query("parcial3(X,Y)"):
	print "Estudiante "+result["X"]+" Nota: "+ str(result["Y"])


print

print "Aprobaron el tercer parcial:"
for result in prolog.query("aprobado3(X)"):
	print result["X"] 

print

print "Aprobaron la materia: "
for result in prolog.query("pasamateria(X,Y)"):
	print result["X"]+" Nota final: "+str(result["Y"])

print

print "Reprobaron la materia: "
for result in prolog.query("pierdemateria(X,Y)"):
	print result["X"]+" Nota final: "+str(result["Y"])



