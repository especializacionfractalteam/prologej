# Prolog como lógica de negocio.

Se realizó una aplicación en Python la cual consulta de un archivo prolog datos sobre estudiantes, sus notas de tres parciales diferentes y su nota final.

## Herramientas usadas:

1.	Python
2.	SWI-prolog
3.	Librería Pyswip, para consultar el archivo de Prolog.

En este caso, Prolog realiza las tareas de:


- Almacenar a los estudiantes.

- Almacenar las notas de los estudiantes para cada uno de los parciales.

- Calcular la nota definitiva de cada estudiante.

En el módulo de Python, se consultan y muestran estos datos.

## Diego Fernando Izquierdo    20201099035
## Mateo Alfonso Puerto        20201099044
