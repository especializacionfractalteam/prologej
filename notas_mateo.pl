estudiante('Diego').
estudiante('Maria').
estudiante('Juan').
estudiante('Diana').

parcial1('Diego',4.4).
parcial1('Maria',5.0).
parcial1('Juan',3.3).
parcial1('Diana',2.4).

parcial2('Diego',3.9).
parcial2('Maria',3.0).
parcial2('Juan',2.3).
parcial2('Diana',4.4).

parcial3('Diego',3).
parcial3('Maria',2.7).
parcial3('Juan',3.4).
parcial3('Diana',2.0).

porcentaje_parcial1(0.25).
porcentaje_parcial2(0.25).
porcentaje_parcial3(0.5).
 
% calculanota(X,A):-   %quiero q aca valla la operacion que tenga la nota final ya con los tres parciales

notafinal(X,Y):-
	X < 3 -> Y = reprobo; 
	true -> Y = aprobo.

reprobado1(X):-  parcial1(X,Z),notafinal(Z,W),W=reprobo.
aprobado1(X):-   parcial1(X,Z),notafinal(Z,W),W=aprobo.    
reprobado2(X):-  parcial2(X,Z),notafinal(Z,W),W=reprobo.
aprobado2(X):-   parcial2(X,Z),notafinal(Z,W),W=aprobo.    
reprobado3(X):-  parcial3(X,Z),notafinal(Z,W),W=reprobo.
aprobado3(X):-   parcial3(X,Z),notafinal(Z,W),W=aprobo.  

notacalculada(E,R):- parcial1(E,X),parcial2(E,Y),parcial3(E,Z), R is (X*0.25)+(Y*0.25)+(Z*0.5).  

pasamateria(X,Z):-notacalculada(X,Z),notafinal(Z,W),W=aprobo.
pierdemateria(X,Z):-notacalculada(X,Z),notafinal(Z,W),W=reprobo.
